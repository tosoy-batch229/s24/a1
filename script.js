// Exponent Operator
let number = 2;
let getCube = number**3;

// Template Literals
console.log(`The cube of ${number} is ${getCube}`);

// Array Destructuring
const address = ["258", "Washington Ave. NW", "California", "90011"];
let [lot, street, state, postalCode] = address;
console.log(`I live at ${lot} ${street} ${state}, ${postalCode}`);

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "Saltwater Crocodile",
	weight: "1075 kgs",
	measurement: "20ft 3in"
}
let {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}`);

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];
numbers.forEach((x) => {
    console.log(x);
})

// Javascript Classes
class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}
let dog1 = new Dog("Luningning", 13, "German Shepherd");
let dog2 = new Dog("Anghel", 3, "Bulldog");
console.log(dog1);
console.log(dog2);